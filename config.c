#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>
#include <jansson.h>

#include "config.h"

//******** Extern Variables **********

//******** Defines and Data Types ****

//******** Function Prototypes *******
static void get_id_from_json(json_t * id_token, uint32_t *id_32bit, uint16_t *id_16bit);

//******** Global Variables **********
config_t config;

//******** Static Variables **********
static config_t def_config = {
	.company_id = 0xFACE,
	.network_key = 0xAABBCCDD,
	.group_ids_len = 0,
	.group_ids = NULL
};


//******** Function Definations ******

/***************************************************
 * Function : load_configuration
 * Description : 
 * Input :
 * Output :
 * Process :
 * sample config file ->
 * {
 *	"config":{
 *		"CompanyId":"FACE",
 *		"NetworkKey":"AABBCCDD",
 *		"GroupID":[
 *		 	{
 *				"groupid":"AAAA",
 *				"nodeid":[
 *				"00A1",
 *				"00A2"
 *				]
 *			},
 *		 	{
 *				"groupid":"BBBB",
 *				"nodeid":[
 *				"00B1",
 *				"00B2"
 *				]
 *			}
 *		]
 *	}
 * }
 * 
 */
int
load_configuration(void) {
	json_error_t error;
	json_t *root = NULL;
	json_t *config_obj;
	json_t *val;
	const char *key;

	root = json_load_file(CONFIG_FILE, 0, &error);
	if(!root) return -1;

	config_obj=json_object_get(root, "config");
	if(!config_obj) return -1;

	json_object_foreach(config_obj, key, val){
		if(!val) {
			json_decref(root);
			return -1;
		}
		switch(json_typeof(val)){
		case JSON_OBJECT:
			break;
		case JSON_ARRAY:
			if (strncmp(key, GROUPID_ARRAY_KEY, strlen(GROUPID_ARRAY_KEY) ) == 0) {
				int index;
				json_t *groupid_obj;
				uint8_t	array_len = json_array_size(val);

				config.group_ids_len = array_len;
				config.group_ids = calloc(array_len, sizeof(group_t) );

				json_array_foreach(val,index,groupid_obj){
					if( groupid_obj && (json_typeof(groupid_obj) == JSON_OBJECT) ){
						json_t *groupid_token = json_object_get(groupid_obj,"groupid");
						json_t *nodeid_array = json_object_get(groupid_obj,"nodeid");

						if ( groupid_token && (json_typeof(groupid_token) == JSON_STRING) ) {
							get_id_from_json(groupid_token, NULL,
							&config.group_ids[index].group_id);
						} else {
							config.group_ids[index].group_id = 0xFFFF; 
						}
						if ( nodeid_array && (json_typeof(nodeid_array) == JSON_ARRAY) ){
							uint8_t	nodeids_len = json_array_size(nodeid_array);
							config.group_ids[index].node_ids_len = nodeids_len;
							printf("nodeids_len=%u\n", nodeids_len);
							config.group_ids[index].node_ids =
								malloc(nodeids_len * sizeof(uint16_t) );
							int idx;
							json_t *nodeid_token;
							json_array_foreach(nodeid_array,idx,nodeid_token){
								if ( nodeid_token && (json_typeof(nodeid_token) ==
									JSON_STRING) ) {
									get_id_from_json(nodeid_token, NULL,
									&(config.group_ids[index].node_ids[idx]) );
								}
							}
						}
					} else {
						free_config();
						json_decref(root);
						return -1;
					}
				}
			}
			break;
		case JSON_STRING: {
			if (strncmp(key, COMPANY_ID_KEY, strlen(COMPANY_ID_KEY) ) == 0) {
				get_id_from_json(val, NULL, &config.company_id);
			} else if (strncmp(key, NETWORK_KEY, strlen(NETWORK_KEY) ) == 0) {
				get_id_from_json(val, &config.network_key, NULL);
			}
		}break;
		default:
			break;
		}
	}

	json_decref(root);
	display_config();
	return 0;
}

void
load_def_config(void) {
	free_config();
	config = def_config;
	display_config();
}

void
free_config(void) {
	for(int i=0; i < config.group_ids_len; i++) {
		if(config.group_ids[i].node_ids) {
			free(config.group_ids[i].node_ids);
		}
	}
	config.group_ids_len = 0;
	free(config.group_ids);
	config.group_ids = NULL;
}

void
display_config(void) {
	printf("{\n\t\"config\":{\n");
	printf("\t\t\"CompanyId\":\"%04X\",\n",config.company_id);
	printf("\t\t\"NetworkKey\":\"%08X\"",config.network_key);
	if(config.group_ids_len) {
	printf(",\n\t\t\"GroupID\":[\n");
	for (int i = 0; i < config.group_ids_len; i++) {
		printf("\t\t\t\{\n");
		printf("\t\t\t\t\"groupid\":\"%04X\",\n", config.group_ids[i].group_id);
		if(config.group_ids[i].node_ids_len) {
		printf("\t\t\t\t\"nodeid\":[\n");
		for (int j=0; j < config.group_ids[i].node_ids_len; j++) {
			printf("\t\t\t\t\t\"%04X\"",config.group_ids[i].node_ids[j]);
			if (j != config.group_ids[i].node_ids_len-1) {
				printf(",");
			}
			printf("\n");
		}
		printf("\t\t\t\t]\n");
		}
		printf("\t\t\t}");
		if(i != config.group_ids_len-1) {
			printf(",");
		}
		printf("\n");
	}
	printf("\t\t]");
	}
	printf("\n\t}\n}\n");
}

uint8_t*
hex_decoder(const char *in, size_t len,uint8_t *out)
{
	unsigned int i, t, hn, ln;

	for (t = 0,i = 0; i < len; i+=2,++t) {
		hn = in[i] > '9' ? toupper(in[i]) - 'A' + 10 : in[i] - '0';
		ln = in[i+1] > '9' ? toupper(in[i+1]) - 'A' + 10 : in[i+1] - '0';

		out[t] = (hn << 4 ) | ln;
	}

	return out;
}

static void
get_id_from_json(json_t * id_token, uint32_t *id_32bit, uint16_t *id_16bit){
	const char *id_string = json_string_value(id_token); 
	char buf[4]={0};
	uint8_t len = (strlen(id_string) > 8) ? 8 : strlen(id_string);
	hex_decoder(id_string, len, (uint8_t *)buf);
	if (id_16bit) {
		*id_16bit =  (buf[0] << 8) | (buf[1] & 0xFF);
	} else if (id_32bit) {
		*id_32bit = 	(buf[0] << 24) |
				( (buf[1] << 16) & 0x00FF0000 ) |
				( (buf[2] << 8) & 0x0000FF00 ) |
				(buf[3] & 0xFF);
	}
}

