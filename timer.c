#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <stdint.h>

#include "timer.h"

//******** Extern Variables **********

//****** Defines and Data Types ******

// e.g. tick 1 is systick and tick 2 is mark tick or
// tick 1 is main tick and tick 2 is a tick which you want to decrement from main tick
#define get_abs_tick_diff(tick1, tick2)	( (tick1 < tick2) ? (tick1 + (0xFFFFFFFF - tick2)) : (tick1 - tick2) )

// 1 ms value, unit is 1 usec
#define VAL_OF_1MS		1000

#define NANO_TO_MILI_SEC	1000000

#define TIME_CNT_IN_MS_FOR_TIMER_PROCESS			100
#define MAX_TIME_CNT_IN_MS_FOR_CONSECUTIVE_PROCESS		300
#define MAX_SYS_TIMER	10

sys_timer_t sys_timers[MAX_SYS_TIMER];

//******** Function Prototypes *******

//******** Global Variables **********

//******** Static Variables **********
static uint32_t sys_tick;

//******** Function Definations ******

/*****************************************************************************
 * get_sys_tick()
 * Param:
 * 	IN :	None
 * 	OUT:	None
 * Returns:	sys_tick - Tick count.
 * Description:
 * 			This function returns current system Tick count.
 * 			This function is useful when timing less then 100ms is required.
*****************************************************************************/
uint32_t 
get_sys_tick(void) {
	uint64_t		curr_msec_cnt;
	struct timespec		time_val;

	//this function read continuous increasing time kernel and it has no relation with realtime in
	//kernel so event though we change time from application it doesn't get affected.
	if(clock_gettime(CLOCK_MONOTONIC, &time_val) == 0)
	{
		curr_msec_cnt = (time_val.tv_sec * VAL_OF_1MS) + (time_val.tv_nsec / NANO_TO_MILI_SEC);
		sys_tick = (curr_msec_cnt % 0xFFFFFFFF);
	}
	else
	{
		printf("clock_gettime failed\n");
	}
	return(sys_tick);
}

/*****************************************************************************
 * init_sys_timer()
 * Param:
 * 	IN :	None
 * 	OUT:	None
 * Returns:	None
 * Description:
 * 			Initialize system timer.
 * 
*****************************************************************************/
void
init_sys_timer(void) {
	sys_tick = 0;
}

/*****************************************************************************
 * run_sys_timer()
 * Param:
 * 	IN : NONE
 * 	OUT: NONE
 * Returns:
 * 	NONE
 * Description:
 * 	Reads system time and derives 1ms count & 100ms count. It updates status
 * 	of all timers every 100ms
 * 
*****************************************************************************/
void
run_sys_timer(void) {
	uint32_t	elaps_tick;
	static uint32_t	prev_sys_tick;
	uint32_t	currection_tick = 0;
	static int	getTickFirstTimeF = 0;

	if(getTickFirstTimeF == 0)
	{
		prev_sys_tick = get_sys_tick();
		getTickFirstTimeF = 1;
		return;
	}

	// Read system time
	elaps_tick = elapsed_tick(prev_sys_tick);

	// Time to service all timers
	if(elaps_tick >= TIME_CNT_IN_MS_FOR_TIMER_PROCESS)
	{
		prev_sys_tick = sys_tick;
		currection_tick = elaps_tick - TIME_CNT_IN_MS_FOR_TIMER_PROCESS;
		// Apply tick correction, if correction tick is greater than 0 msec and less than
		// 'MAX_TIME_CNT_IN_MS_FOR_CONSECUTIVE_PROCESS'. Upper limit check is MUST otherwise change in date time
		// may result into large correction tick which will create issue for calcuting previous tick.
		if( (currection_tick >= 0) && (currection_tick < MAX_TIME_CNT_IN_MS_FOR_CONSECUTIVE_PROCESS) )
		{
			prev_sys_tick = get_abs_tick_diff(prev_sys_tick, currection_tick);
			for (int i=0; i < MAX_SYS_TIMER; i++) {
				if(sys_timers[i].busy) {
					sys_timers[i].current_count++;
					if(sys_timers[i].current_count > sys_timers[i].count) {
						sys_timers[i].current_count = 0;
						sys_timers[i].cb(sys_timers[i].data);
					}
				}
			}
		}
	}
}

//*****************************************************************************
//	DeleteTimer()
//	Param:
//		IN :	handle
//		OUT:	None
//	Returns:	None
//	Description:
//				Delete timer as per given handle.
//
//*****************************************************************************
void
delete_timer(uint8_t timer_idx) {
	if (timer_idx < MAX_SYS_TIMER) {
		sys_timers[timer_idx].busy = 0;
	}
}

//*****************************************************************************
//	elapsed_tick()
//	Param:
//		IN :	mark_tick - tick count with which to check
//		OUT:	None
//	Returns:	elasped tick
//	Description:
//				Returns difference between current system tick count and
//				specified tick. Application can use this function to find
//				elapsed tick since the last mark.
//*****************************************************************************
uint32_t
elapsed_tick(uint32_t mark_tick) {
	// First, update system tick
	get_sys_tick();
	return(get_abs_tick_diff(sys_tick, mark_tick));
}

uint8_t
start_timer(sys_timer_t timer_info) {
	int i=0;
	for (; i < MAX_SYS_TIMER; i++) {
		if (!sys_timers[i].busy) {
			break;
		}
	}
	if ( i < MAX_SYS_TIMER) {
		sys_timers[i] = timer_info;
		sys_timers[i].busy = 1;
		return i;
	}
	return -1;
}

