# TOOL

This tool will help you to send command(moveup/movedown) to shades and get response from it.

# BUILD
	# make clean all


# CONFIG
Create config.json as below edit NetworkKey, groupid , nodeid as per your setup.

    {
    	"config":{
    		"CompanyId":"01D9",
    		"NetworkKey":"00000001",
    		"GroupID":[
    			{
    				"groupid":"0002",
    				"nodeid":[
    					"0003",
    					"0004"
    				],
    				"groupid":"0005",
    				"nodeid":[
    					"0006",
    					"0007"
    				],
    			}
    		]
    	}
    }
  
# HELP
To get help of how to use tool use below command

    # ./tool help
    ./tool hostIP command(moveup/movedown) speed node_id

## COMMAND
* MOVE UP
	    ./tool hostIP moveup speed node_id
	    #Example
	    ./tool 192.168.1.9 moveup 1 0002

* MOVE DOWN
	    ./tool hostIP movedown speed node_id
	    #Example
	    ./tool 192.168.1.9 movedown 1 0005


