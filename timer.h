#if !defined TIMER_H
#define TIMER_H

typedef struct {
	uint32_t count;
	uint32_t current_count;
	void     (*cb)(uint32_t data);
	uint32_t data;
	uint8_t  busy;
}sys_timer_t;

uint32_t get_sys_tick(void);
void init_sys_timer(void);
void run_sys_timer(void);
void delete_timer(uint8_t timer_idx);
uint32_t elapsed_tick(uint32_t mark_tick);
uint8_t start_timer(sys_timer_t timer_info);

#endif
