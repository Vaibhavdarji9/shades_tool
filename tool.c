#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>

#include "config.h"
#include "ilumi_def.h"
#include "timer.h"

//******** Extern Variables **********
extern config_t config;

//******** Defines and Data Types ****
#define MAX_EXPECTED_NODE_ID 20

typedef enum {
	STOP,
	START
}start_stop_enum_e;

//******** Function Prototypes *******

//******** Global Variables **********
struct sockaddr_in g_client_name;
int g_fd1, g_fd2;
uint8_t g_cmd_msg_type;
uint8_t g_seq_num;
uint16_t g_node_id;
uint8_t g_cmd;
uint8_t retry;
uint16_t expected_node_id[MAX_EXPECTED_NODE_ID]; 

//******** Static Variables **********

//******** Function Definations ******

int
socket_init(char *hostname) {
	int fd, fd2;
	int ret;
	struct sockaddr_in my_name, my_name2, client_name;
	int broadcastPermission; /* Socket opt to set permission to broadcast */
	struct  hostent  *ptrh;  /* pointer to a host table entry */
	
	printf("hostname=%s\n",hostname);

	/* create a socket */
	fd = socket(AF_INET, SOCK_DGRAM, 0);
	g_fd1 = fd;
	if (fd == -1) {
		printf("creating socket failed\n");
		return -1;
	}

	/* server address */
	bzero(&my_name, sizeof(my_name));
	my_name.sin_family = AF_INET;
	my_name.sin_addr.s_addr = INADDR_ANY;
	ret = bind(fd, (struct sockaddr *) &my_name, sizeof(my_name));
	if (ret) {
		fprintf(stderr, "bind: %d; errno %d: %s\n", ret, errno, strerror(errno));
		return -1;
	}

	/* make socket non blocking */
	ret = fcntl(fd, F_SETFL, O_NONBLOCK);
	if (ret) {
		fprintf(stderr, "fcntl: %d; errno %d: %s\n", ret, errno, strerror(errno));
		return -1;
	}

	/* Set socket to allow broadcast */
	broadcastPermission = 1;
	if (setsockopt(fd, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission,
	       sizeof(broadcastPermission)) < 0) {
		perror("setsockopt() failed");
	}

	/* create a second socket for non-control messages */
	fd2 = socket(AF_INET, SOCK_DGRAM, 0);
	g_fd2 = fd2;
	if (fd2 == -1) {
		printf("creating socket for non control msg failed\n");
		return -1;
	}

	/* server address */
	bzero(&my_name2, sizeof(my_name2));
	my_name2.sin_family = AF_INET;
	my_name2.sin_addr.s_addr = INADDR_ANY;
	ret = bind(fd2, (struct sockaddr *) &my_name2, sizeof(my_name2));
	if (ret) {
		fprintf(stderr, "bind2: %d; errno %d: %s\n", ret, errno, strerror(errno));
		return -1;
	}

	/* make socket non blocking */
	ret = fcntl(fd2, F_SETFL, O_NONBLOCK);
	if (ret) {
		fprintf(stderr, "fcntl2: %d; errno %d: %s\n", ret, errno, strerror(errno));
		return -1;
	}

	client_name.sin_family = AF_INET;
	client_name.sin_port = htons(12005);

	/* Convert host name to equivalent IP address and copy to name */
	ptrh = gethostbyname(hostname);
	if ( ((char *)ptrh) == NULL ) {
		fprintf(stderr,"Invalid host: %s\n", hostname);
		return -1;
	}
	memcpy(&client_name.sin_addr, ptrh->h_addr, ptrh->h_length);
	g_client_name = client_name;
	return 0;
}

static int
difftimeofday_sec(struct timeval *startTimeP, struct timeval *endTimeP)
{
	int secs = 0;
	if (startTimeP->tv_sec < endTimeP->tv_sec) {
		long totaluS = ((1000 * 1000) - startTimeP->tv_usec) + endTimeP->tv_usec;
		totaluS += 1000 * 1000 * (endTimeP->tv_sec - startTimeP->tv_sec);
		secs = totaluS / (1000 * 1000);
	}

	return secs;
}
static int
send_downlink_message(int fd, struct sockaddr_in *client_nameP) {
	char buf4[] = {0x03, 0x50, 0x05, 0x01 };
	int size = 0;
	int len = 0;
	int ret = 0;

	size = sizeof(buf4);
	for (len=0; len < size; ) {
		ret = sendto(fd, buf4+len, size-len, 0, (struct sockaddr *) client_nameP, sizeof(*client_nameP));
		if (ret <= 0) {
			return -1;
		}
		len += ret;
	}
	return 1;
}
static int
send_uplink_message(int fd, struct sockaddr_in *client_nameP, int secs) {
	char buf4[] = {0x04, 0x50, 0x05, 0x00, 0x01 };
	unsigned char readBuf[40];
	int readCount = 0;
	int ret = 0;
	int i;
	int size = 0;
	int len = 0;
	unsigned int addrlen = sizeof(*client_nameP);
	struct timeval tv1;

	size = sizeof(buf4);
	for (len=0; len < size; ) {
		ret = sendto(fd, buf4+len, size-len, 0, (struct sockaddr *) client_nameP, sizeof(*client_nameP));
		if (ret <= 0) {
			return -1;
		}
		len += ret;
	}

	readCount = 0;

	// Get the Response
	gettimeofday(&tv1, NULL);
	while (readCount < 5) {
		ret = recvfrom(fd, &readBuf[readCount], sizeof(readBuf)-readCount, 0, (struct sockaddr *) client_nameP, &addrlen);
		if (ret > 0) {
			if (secs) printf("\n");
			printf("Uplink Response:");
			for (i = 0; i < ret; i++) {
				printf("%x ", readBuf[readCount] & 0xff);
				readCount++;
			}
			printf("\n");
		} else if ((ret < 0) && (errno != EAGAIN)) {
			printf("Uplink Response read err %d %s\n", errno, strerror(errno));
		} else {
			//printf("jdb *read[%d]:%d \n", readCount, ret);
		}

		if (secs) {
			struct timeval tv2;
			gettimeofday(&tv2, NULL);
			if (difftimeofday_sec(&tv1, &tv2) > secs) {
				return 0; // timeout
			}
		}
	}

	// Check the Response
	if (((readBuf[1] & 0xff) == 0xC0) && ((readBuf[2] & 0xff) == 0x13)) {
		// pass thru log is ok
		return 0; // timeout
	} else if ((readBuf[1] & 0xff) != 0x50) {
		printf("Bad Uplink Response %x-%x\n", readBuf[1] & 0xff, readBuf[2] & 0xff);
		return -1;
	}

	return 1;
}

void
print_buf(char *tag, uint8_t *buf, uint16_t size) {
	uint16_t idx;

	if ( !tag || !buf || !size ) return;

	printf("%s: ",tag);
	for (idx=0; idx < size; idx++) {
		printf("%02X ", buf[idx] );
	}
	printf("\n");
}

int
start_stop_scan(int fd, struct sockaddr_in *client_nameP, uint8_t start_stop) {
	uint8_t buf[7] = { 6, 0x81, 0x02, 0x01, 0x11, 0x27, start_stop};
	int size=7;
	int len=0;
	int ret=0;

	print_buf("start scan", buf, size);
	for (len=0; len < size; ) {
		ret = sendto(fd, buf+len, size-len, 0, (struct sockaddr *) client_nameP, sizeof(*client_nameP));
		if (ret <= 0) {
			return -1;
		}
		len += ret;
	}
	return 0;
};

uint8_t cal_crc(uint8_t *data, uint8_t len) {
	if (data == NULL) return 0;
	uint8_t crc = 0xFF;

	for (uint8_t i=0; i < len; i++) {
		crc ^= data[i];
//		printf("crc=0x%X data=0x%0X\n",crc, data[i]);
	}
	return crc;
}
int
send_command (uint8_t cmd, int fd, struct sockaddr_in *client_nameP, uint16_t node_id, uint8_t speed) {
	iLumi_adv_api_id_t adv_cmd;
	uint8_t buf[64];
	uint8_t cmd_header[] = {0x1d, 0x81, 0x00, 0x00,0x00,0x00,0x00,0x00,0x00 };
	uint8_t move_up[]   = { 0x55, 0x01, 0x03, 0x01, 0x01, 0x01, 0x01, 0xFF };
	uint8_t move_down[] = { 0x55, 0x01, 0x03, 0x03, 0x01, 0x01, 0x01, 0xFF };
	uint8_t  *api_payload = NULL;

	move_up[6] = speed;
	move_down[6] = speed;

	move_up[7] = cal_crc(&move_up[1], 6);
	move_down[7] = cal_crc(&move_down[1], 6);

	adv_cmd.ilumi_id = config.company_id;
	adv_cmd.msg_type = ILUMI_AD_MSG_PROXY_API_GROUP_ID;
	adv_cmd.ttl_seq_num = 0x40 | (g_seq_num & 0x0F);
	adv_cmd.group_node_id = ( (config.network_key >> 16) ^ node_id);
	adv_cmd.payload_size = 6+9;
	adv_cmd.api_seq_num = g_seq_num & 0xFF;
	adv_cmd.api_msg_type = ILUMI_API_QUERY;
	adv_cmd.node_id = 0xFFFF;
	adv_cmd.query_type = ILUMI_QUERY_SET_MOTOR_MSG;
	adv_cmd.start_index = 8;

	if (cmd == 0) {
		api_payload = move_up;
	} else {
		api_payload = move_down;
	}

	g_cmd_msg_type = ILUMI_AD_MSG_PROXY_API_GROUP_ID;
	g_seq_num++;

	memcpy(buf, cmd_header, sizeof(cmd_header) );
	buf[sizeof(cmd_header)] = sizeof(adv_cmd) + 9 + 2;
	buf[sizeof(cmd_header) + 1] = 0xFF;

	memcpy(buf+sizeof(cmd_header)+2, &adv_cmd, sizeof(adv_cmd) );
	if (api_payload)
		memcpy(buf+sizeof(cmd_header)+sizeof(adv_cmd)+2, api_payload, 8);

	int size = 0;
	int len = -1;
	int ret = 0;

	size = sizeof(cmd_header) + sizeof(adv_cmd) + adv_cmd.payload_size;
	buf[0] = 0x20;
	print_buf("command", buf, size);

	for (len=0; len < size; ) {
		ret = sendto(fd, buf+len, size-len, 0, (struct sockaddr *) client_nameP, sizeof(*client_nameP));
		if (ret <= 0) {
			return -1;
		}
		len += ret;
	}
	if (start_stop_scan(fd, client_nameP, START) != 0) {
		return -1;
	}
	return 0;
}

int
send_uplink_message_wait(int fd, struct sockaddr_in *client_nameP)
{
	int rtn = 0;

	printf("Uplinking\n");
	fflush(NULL);
	while (1) {
		int ret = send_uplink_message(fd, client_nameP, 3); // try every 3 seconds
		if (ret < 0) {
			rtn = -1;
			break;
		}
		if (ret > 0) break;
		printf(".");
		fflush(NULL);
	}

	return rtn;
}

int
check_ilumi_package_and_validate (uint8_t *buf, uint8_t len, uint16_t t_node_id) {
	// FF CE FA 50 4B 22 33 06 8B 73 A2 00 19
	//printf("check_ilumi_package_and_validate: %02X %02X %02X %02X\n", buf[0], buf[3], buf[9], buf[8]);

	printf("<<<<<<<<<\n");
	uint16_t company_id = (buf[2] << 8) | (buf[1] & 0xFF);
	uint16_t node_id = (buf[11] << 8) | (buf[10] & 0xFF);
	int i = 0;
	if (node_id == 0) {
		return -1;
	}
	if (company_id != config.company_id){
		printf("expected company_id=%04X and received=%04X\n", config.company_id, company_id);
		return -1;
	}
	if (buf[0] != 0xFF) {
		printf("ADV MSG type is not  0xFF\n");
		return -1;
	}
	if (buf[3] != g_cmd_msg_type) { 
		printf("received ilumi_adv_msg_type is 0x%02X not equal to expected 0x%02X\n", buf[3], g_cmd_msg_type);
		return -1;
	}
	if (buf[9] != ILUMI_API_QUERY_RESPONSE) {
		printf("received ilumi_api_msg_type is 0x%02X not equal to expected 0x%02X\n", buf[9], ILUMI_API_QUERY_RESPONSE);
		return -1;
	}
	if (buf[8] != ((g_seq_num-1) & 0xFF) ) {
		printf("seq num is expected=%u and received=%u\n", (g_seq_num-1) & 0xFF, buf[8]);
		return -1;
	}

	printf("Received data from node_id=%04X\n", node_id);
	if(t_node_id != 0xFFFF) {
		int count = 0;
		printf("command sent to node/group id=%04X\n", t_node_id);
		for (; i < MAX_EXPECTED_NODE_ID; i++) {
			if (node_id == expected_node_id[i]) {
				printf("expected_node_id[%d]=%04X\n", i, expected_node_id[i]);
				print_buf("received_data", buf, len);
				expected_node_id[i] = 0;
				break;
			}
		}
		if (i >= MAX_EXPECTED_NODE_ID) {
			return -1;
		}
		for (i=0; i < MAX_EXPECTED_NODE_ID; i++) {
			if (expected_node_id[i] != 0) {
				printf("wating for response from node_id = %04X\n", expected_node_id[i]);
				count++;
			}
		}
		if(count > 0)
			return -1;
	}

	return 0;
}

int
receive_response(uint16_t node_id) {
	uint8_t readBuf[256]; //expanded for ble testing
	unsigned int addrlen = sizeof(g_client_name);
	int ret;
	struct sockaddr_in client_name = g_client_name;
	int fd=g_fd1, fd2=g_fd2;

	//
	// CTRL
	//
	ret = recvfrom(fd, readBuf, sizeof(readBuf), 0, (struct sockaddr *) &client_name, &addrlen);
	if (ret > 0) {
//		int i;
//		for (i = 0; i < ret; i++) {
//			printf("%02X ", readBuf[i] & 0xff);
//		}
//		printf("\n");

		if ((readBuf[1] == 0x50) && (readBuf[2] == 0x06)) {
			// respond to transmission control packet to keep uplink active
			sendto(fd, readBuf, 4, 0, (struct sockaddr *) &client_name, sizeof(client_name));
		} else if ((readBuf[1] == 0xC0) && (readBuf[2] == 0x13)) { // Log
			sendto(fd, readBuf, 4, 0, (struct sockaddr *) &client_name, sizeof(client_name));
		} else if ((readBuf[1] == 0x81) && (readBuf[2] == 0x09)) {
			// 34
			// 81 09
			// A2 4E 56 C0 78 F9
			// B9
			// 02 01 06 
			// 0E FF CE FA 50 4B 22 33 06 8B 73 A2 00 19
			if (check_ilumi_package_and_validate(readBuf+14, readBuf[13], node_id) == 0) {
				start_stop_scan(fd, (struct sockaddr_in *) &g_client_name, 0);
				return 0;
			}
		}
		fflush(NULL);
	} else if ((ret < 0) && (errno != EAGAIN)) {
		printf("read err %d %s\n", errno, strerror(errno));
	}

	//
	// MSGS
	//
	ret = recvfrom(fd2, readBuf, sizeof(readBuf), 0, (struct sockaddr *) &client_name, &addrlen);
	if (ret > 0) {
		int i;
		printf("fd_2\n");
		for (i = 0; i < ret; i++) {
			printf("%02X", readBuf[i] & 0xff);
		}
		printf("\n");

		if ((readBuf[1] == 0xD3) && (readBuf[2] == 0x04)) {
			printf("set lamp done, end.\n");
			fflush(NULL);
			return -1;
		} else if ((readBuf[1] == 0x42) && (readBuf[2] == 0x02)) {
			// respond to gpio input level change packet to stop retries
			sendto(fd2, readBuf, 5, 0, (struct sockaddr *) &client_name, sizeof(client_name));
		} else if ((readBuf[1] == 0x81) && (readBuf[2] == 0x09)) {
			printf("broadcast message received\n");
		}

		fflush(NULL);
	} else if ((ret < 0) && (errno != EAGAIN)) {
		printf("read err %d %s\n", errno, strerror(errno));
	}
	return -1;
}

void
retry_sending_command(uint32_t speed) {
	printf("retry_sending_command\n");
	if (--retry) {
		send_command(g_cmd, g_fd1, (struct sockaddr_in *) &g_client_name, g_node_id, (uint8_t)speed);
		printf("retry %u\n", retry);
	} else {
		send_downlink_message(g_fd1, (struct sockaddr_in *) &g_client_name);
		free_config();
		exit(-1);
	}
}

int
main(int argc, char *argv[]) {
	char *hostname = "192.168.1.26";
	char *command = "moveup";
	char *node_id_string = "FFFF";
	uint8_t cmd=0;
	uint8_t speed=1;
	uint16_t node_id = 0xFFFF;
	uint8_t buf[32];

	switch(argc) {
		case 2:{
			hostname = argv[1];
			if (strcmp(argv[1], "help") == 0) {
				printf("./tool hostIP command(moveup/movedown) speed target_node_id\n");
				return 0;
			}
		}break;
		case 3:{
			hostname = argv[1];
			command = argv[2];
		}break;
		case 4:{
			hostname = argv[1];
			command = argv[2];
			speed = atoi(argv[3]);
		}break;
		case 5:{
			hostname = argv[1];
			command = argv[2];
			speed = atoi(argv[3]);
			node_id_string = argv[4];
		}break;
		default:{
		}break;
	}

	int str_len = (strlen(node_id_string) > 4) ? 4 : strlen(node_id_string);

	hex_decoder(node_id_string, str_len, buf);
//	printf("node_id_string=%s buf[0]=%02X buf[1]=%02X\n", node_id_string, buf[0], buf[1]);	
	if(str_len > 2) {
		node_id = ((uint16_t)buf[0] << 8) | (buf[1] & 0xFF);
	} else {
		node_id = buf[0];
	}

	printf("hostname=%s command=%s speed=%u node_id=0x%04X\n", hostname, command, speed, node_id);

	if (load_configuration() != 0) {
		load_def_config();
	}

	if (socket_init(hostname) != 0) {
		printf("socet_init failed\n");
		return -1;
	}

	init_sys_timer();

	srand(time(0));

	send_uplink_message_wait(g_fd1, (struct sockaddr_in *) &g_client_name);

	if (strcmp(command, "moveup") == 0) {
		cmd=0;
	} else if (strcmp(command, "movedown") == 0) {
		cmd=1;
	} else {
		return -1;
	}

	g_seq_num = (rand() & 0xFF);

	if (node_id != 0xFFFF) {
		int i=0;
		for (; i < config.group_ids_len; i++) {
			if (config.group_ids[i].group_id == node_id) {
				printf("broadcasting command to group id = 0x%0X\n",node_id);
				for (	int j=0;
					(j < config.group_ids[i].node_ids_len ) &&
					(j < MAX_EXPECTED_NODE_ID );
					j++) {
					expected_node_id[j] = config.group_ids[i].node_ids[j];
					printf("expected_node_id[%d]=%04X\n", j, expected_node_id[j]);
				}
				break;
			}
		}
		if (i >= config.group_ids_len) {
			expected_node_id[0] = node_id;
		}
	} else {
		printf("node id is 0xFFFF so we are broadcasting command to all the shades\n");
	}

	retry = 3;
	send_command(cmd, g_fd1, (struct sockaddr_in *) &g_client_name, node_id, speed);
	g_cmd = cmd;
	g_node_id = node_id;

	sys_timer_t timer_info = { 100, 0, retry_sending_command, speed, 1};
	start_timer(timer_info);

	while (1) {
		run_sys_timer();
		if (receive_response(node_id) == 0) {
			break;
		}
	}
	send_downlink_message(g_fd1, (struct sockaddr_in *) &g_client_name);
	free_config();

	return 0;
}
