all:
	gcc -Wall -Werror tool.c config.c timer.c -ljansson -o tool

clean:
	rm -f tool

static:
	gcc -Wall -Werror tool.c config.c timer.c -ljansson -o tool.static -static
