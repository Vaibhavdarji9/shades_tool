#if !defined ILUMICONFIG_H
#define ILUMICONFIG_H

//******** Place all include files ***

//******** Defines and Data Types ****
#define CONFIG_FILE "config.json"
#define COMPANY_ID_KEY "CompanyId"
#define NETWORK_KEY "NetworkKey"
#define GROUPID_ARRAY_KEY "GroupID"

typedef struct {
	uint16_t group_id;
	uint8_t node_ids_len;
	uint16_t *node_ids;
}group_t;

typedef struct {
	uint16_t company_id;
	uint32_t network_key;
	uint8_t group_ids_len;
	group_t *group_ids;
}config_t;

//******** Function Prototypes *******
void display_config(void);
int load_configuration(void);
void free_config(void);
void load_def_config(void);
uint8_t* hex_decoder(const char *in, size_t len,uint8_t *out);

#endif
